import groovy.transform.CompileStatic
import groovy.transform.TailRecursive

@CompileStatic
class MapReducer {
  static List<String> prepare(String s) {
     s.split("\\s+") as List<String>
  }
  
  static List<Tuple> map(List<String> strs, List<Tuple> ret = []) {
    if (strs.empty) ret
    else {
      ret << new Tuple(strs.head(), 1)
      map(strs.tail(), ret)
    }
  }
  
  @TailRecursive
  static Map<String, Integer> reduce(List<Tuple> input, Map<String, Integer> ret = [:]) {
    if (input.empty) ret
    else {
      Tuple current = input.head()
      
      String key = current[0]
      Integer val = ret.get(key) ?: 0
      ret.put(key, val + (current[1] as Integer))
      reduce(input.tail(), ret)
    }
  }
}
MapReducer mr = new MapReducer()
String input = "a a a    b b c a b c    c a"
List<String> stringlist = mr.prepare(input)

List<Tuple> map = mr.map(stringlist)
Map<String, Integer> mResult = mr.reduce(map)